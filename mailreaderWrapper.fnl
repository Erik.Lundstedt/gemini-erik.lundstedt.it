#!/bin/env fennel
;; -*- indent-tabs-mode: always; tab-width: 4; global-whitespace-mode: t; -*-
(var debug "false")



(local gitDir		"/home/erik/gemini")
(local gotoGit		["cd " gitDir " ; "] )
(local commitArgs	["-a" "--no-gpg-sign" "-m"  "\"" (os.time) "\"" ])
;;"--allow-empty "


(local status		[(table.concat gotoGit " ") "git" "status"                               ])
(local gitPull		[(table.concat gotoGit " ") "git" "pull"                                 ])
(local gitPush		[(table.concat gotoGit " ") "git" "push" "--no-signed"                   ])
(local gitCommit	[(table.concat gotoGit " ") "git" "commit" (table.concat commitArgs " ") ])

(fn printerr [args]
	(if (= debug "true")
		(print args)
		)
	)



;;"tightly wraps os.execute but handles  1d table and not just a string"
(fn exec [args]
	(os.execute (table.concat args " "))
)

(exec ["killall " "agate"])
(printerr "get status")
(exec status)
(printerr "run git pull")
(exec gitPull)
(printerr "run java mailfetcher")
(exec ["java" "-jar" "$HOME/bin/javaMailReader.jar " ">"])
(exec [(table.concat gotoGit)  (.. "echo " (os.time) ) ">" "test.txt "])
(printerr "add everything to be commited")
(exec [(table.concat gotoGit)  "git" "add" "."])
(printerr "run git commit")
(exec gitCommit)
(printerr "run git push")
(exec gitPush)
(exec [(table.concat gotoGit) "./syncAndServe"])
;;end
